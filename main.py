from kivy.app import App
from kivy.uix.gridlayout import GridLayout

#from MenuMap import MenuMapController

from menuAccueil import MenuAccueilController


class Fenetre(GridLayout):
    def __init__(self, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)

    def afficher_menu(self, menu):
        self.clear_widgets()
        self.add_widget(menu)



class Menus():
    def __init__(self, **kwargs):
        self.fenetre = Fenetre()
        self.menuAccueil = MenuAccueilController(self)
        self.menu_map = MenuMapController(self)

        self.fenetre.afficher_menu(self.menuAccueil)

    def afficher_menu(self, menu):
        self.fenetre.afficher_menu(menu.vue)






class MyApp(App):

    def build(self):
        menus = Menus()
        return menus.fenetre



if __name__ == "__main__":
    MyApp().run()
