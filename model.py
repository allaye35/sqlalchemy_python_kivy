import sqlalchemy as sqlalchemy
from sqlalchemy import create_engine, Column, Integer, String,or_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
engine= create_engine("mysql://root:root@localhost:3306/student", echo=False)
session= sessionmaker(bind=engine)
session=session()
Base= declarative_base()


class Voix(Base):
    __tablename__="voix"
    id=sqlalchemy.Column(Integer, primary_key=True)
    aigus=Column(Integer)
    graves=Column(Integer)
    puissance = Column(Integer)

class Animal(Base):
    __tablename__="animal"
    id=sqlalchemy.Column(Integer, primary_key=True)
    nom=Column(String(50))
    age=Column(Integer)
    sexe = Column(String(50))



class Homme(Base):
    __tablename__="homme"
    id=sqlalchemy.Column(Integer, primary_key=True)
    prenom=Column(String(50))
    chiens = Column(String(50))
    chiens_tues = Column(String(50))
    chiens_suicides = Column(String(50))



class Chien(Base):
    __tablename__="chien"
    id=sqlalchemy.Column(Integer, primary_key=True)
    maitre=Column(String(50))
Base.metadata.create_all(engine)


session.add_all([])
session.commit()