import json

from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from sqlalchemy.orm import session

from model import Chien, Homme, Voix



class MenuAccueilVue(GridLayout):
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.controller = controller
        self.grid1 = GridLayout(cols=4)
        self.input_prenom = None
        self.add_widget(self.grid1)
        self.scrowllView_1=ScrowllView
        self.add_widget(self.scrowllView_1)
        self.scrowllView_2 = ScrowllView
        self.add_widget(self.scrowllView_2)
        self.liste_hommes =[]
        self.liste_chiens = []
        self.age = None
        self.comptes = {"personnages": []}
        self.charger()
        self.layout1()


    def layout1(self):

        self.grid1.add_widget(Label(text="liste_hommes:"))
        self.grid1.add_widget(self.lastName)
        self.grid1.add_widget(Label(text="liste_chiens"))
        self.grid1.add_widget(self.firstName)


        valider = UI.Bouton(text="Crée_un_compte")
        valider.bind(on_press=self.sauvegader_jeu_identifiant_joueur)
                     #on_release=self.lecteur_De_Fichier_compte_Sauvegarder)
        self.grid1.add_widget(valider)

       #self.input_prenom = TextInput()
       #self.grid1.add_widget(self.input_prenom)

        boutton_creation_homme = UI.Bouton(text="créer_homme")
        self.scrowllView_1.add_widget(boutton_creation_homme)
        boutton_creation_homme.bind(on_press=self.connecter)

        boutton_creation_chien = UI.Bouton(text="créer_chien")
        self.scrowllView_2.add_widget(boutton_creation_chien)
        boutton_creation_chien.bind(on_press=self.connecter)

    def clbk_charge_info_homme(self):
        self.liste_hommes=self.controller.charge_info_homme()

    def clbk_charge_info_hien(self):
        self.liste_chiens = self.controller.charge_info_hien()



class MenuAccueilController():
    def __init__(self, menus):
        self.menus = menus
        self.vue = MenuAccueilVue(self)
        self.personnages = []
        self.homme=[]
        self.animal=[]
        self.voix=[]
        self.chien=[]

    def naviguer_menu_map(self):
        self.menus.afficher_menu(self.menus.menu_map)


    def enregistrer_utilisateur(self, prenom):
        p = Personnage.Personnage(prenom)
        print("Utilisateur enregistré : ", prenom)
        self.personnages.append(p)


    def creation_de_voix(self):
        voix = Voix(id=1, aigus=3,graves=5,  puissance =60)
        self.voix.append(voix)
        session.add(voix)
        #session.add_all([voix])
        return voix

    def creation_de_personne(self):
        voix=session.query(self.creation_de_voix())
        homme = Homme(id=1, prenom='TOTO',chiens='TITI', chiens_tues='TATA',  chiens_suicides='TETE')
        self.homme.append(homme)


    def creation_de_chien(self):
        voix = session.query(self.creation_de_voix())
        chien = Chien(id=1,maitre='h1')
        self.chien.append(chien)
        session.add(chien.voix)

    """def ajout_de_voix_sur_homme(self):
        homme=None
        voix=None
        for homme in self.homme:
             homme=homme[0]

        for voix in self.voix:
            voix=voix[0]
        self.homme_avec_voix=homme.voix
        return self.homme_avec_voix

    def ajout_de_voix_sur_chien(self):
        chien = None
        voix = None
        for chien in self.chien:
            chien = chien[0]

        for voix in self.voix:
            voix = voix[0]
        self.chien_avec_voix = chien.voix
        return self.chien_avec_voix"""

    def charge_info_homme(self):
        homme = session.query(Homme)
        return homme

    def charge_info_hien(self):
        chien = session.query(Chien)

        return chien

    def homme_tue_son_chien(self,id_chien,  booleen=False):
        tue = booleen
        if tue == True:
            chien = session.query(Homme).filter(Homme.id == id_chien).first()
            session.delete(chien)
            session.commit()
            # todo on peut utiluser le self ici par exemple self.hiens_tues +=1
            compteur_chiens_tues = session.query(Homme)  # .filter(Homme.chiens_tues = chiens_tues + 1 ).first()
            compteur_chiens_tues.chien_tues = compteur_chiens_tues.chien_tues + 1
            session.commit()

    def chien_se_suicide(self, id_chien, booleen=False):
        se_suicide = booleen
        if se_suicide == True:
            chien = session.query(Homme).filter(Homme.id == id_chien).first()
            session.delete(chien)
            session.commit()
            # todo on peut utiluser le self ici par exemple self.chiens_suicides +=1
            compteur_chiens_suicides = session.query(Homme)  # .filter(Homme.chiens_suicides = chiens_suicides + 1 ).first()
            compteur_chiens_suicides.chiens_suicides = compteur_chiens_suicides + 1
            session.commit()
